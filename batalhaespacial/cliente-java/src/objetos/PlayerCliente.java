package objetos;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PlayerCliente {
    private byte[] ID;

    private String nomeJogador;
    private String nomeNave;

    private int posicaoHorizontal = 0;
    private int posicaoVertical = 0;
    private int vidas = 3;

    /**
     * Criar um jogador para ser utilizado pelo SocketWar. Sendo necessário ao criar um jogador cadastrar o nome da sua
     * name e o nome do jogador.
     *
     * @param nomeJogador No jogador.
     * @param nomeNave Nome da nave do jogador.
     * @throws IOException Erro disparado quando se apresenta algum tipo de problema com o nome do jogador ou nome da nave.
     */
    public PlayerCliente(String nomeJogador, String nomeNave) throws IOException {
        //Lança exceção caso o nome da nave ou do jogador são nulos/vazios.
        if ((nomeJogador == null || nomeJogador.isEmpty()) ||
            (nomeNave == null || nomeNave.isEmpty())) {
            throw new IOException("Nome do jogador e da nave não podem ser nulos/vazios");
        }
    }

    /**
     * Define a posição horizontal do jogador.
     * @param x posição horizontal do jogador.
     */
    public void setPosicaoHorizontal (int x) {
        posicaoHorizontal = x;
    }

    /**
     * Define a posição vertical do jogador.
     * @param y posição vertical do jogador.
     */
    public void setPosicaoVertical (int y) {
        posicaoVertical = y;
    }

    /**
     * Define as vidas do jogador.
     * @param vidas quantidade de vidas do jogador.
     */
    public void setVidas (int vidas) {
        this.vidas = vidas;
    }

    /**
     * Obtém a atual posição horizontal (x) do jogador.
     * @return Posição Horizontal (x) do jogador.
     */
    public int getPosicaoHorizontal () {
        return posicaoHorizontal;
    }

    /**
     * Obtém a atual posição vertical (y) do jogador.
     * @return Posição Vertical (y) do jogador.
     */
    public int getPosicaoVertical () {
        return posicaoVertical;
    }

    /**
     * Obtém a quantidade de vidas do jogador.
     * @return Quantidade de vidas do jogador.
     */
    public int getVidas() {
        return vidas;
    }

    /**
     * Obtém o ID do jogador.
     * @return ID do jogador.
     */
    public byte[] getID() {
        return ID;
    }

    /**
     * Obtém o nome do jogador cadastrado no Player.
     * @return Nome do jogador.
     */
    public String getNomeJogador() {
        return nomeJogador;
    }

    /**
     * Obtém o nome da nave cadastrada no Player.
     * @return Nome da nave.
     */
    public String getNomeNave() {
        return nomeNave;
    }
}
