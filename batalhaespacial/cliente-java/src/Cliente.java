import objetos.PlayerCliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

public class Cliente {

    private static int INTERVALO_DIGITACAO = 30;
    private static String LOCALHOST_ANDRESS = "127.0.0.1";
    private static String PORTA_PADRAO = "5000";
    private static int TAMANHO_MENSAGEM = 1024;

    private static String REGISTRO_JOGADOR = "@registro";

    private static PlayerCliente jogador;
    private static String enderecoServidor;
    private static int portaDeAcesso;

    public static void main(String args[]) throws IOException {
        slowPrint("Bem Vindo!\n", INTERVALO_DIGITACAO);
        slowPrint("SocketWar logo ira começar.\n\n\n", INTERVALO_DIGITACAO);

        enderecoDeAcesso();
        criaJogador();
        registraJogador();

//        BufferedReader inputDoUsuario = new BufferedReader(new InputStreamReader(System.in));
//
//        DatagramSocket clienteSocket = new DatagramSocket();
//        InetAddress IPAdress = InetAddress.getByName("localhost");
//
//        byte[] sendData = new byte[1024];
//        byte[] receiveData = new byte[1024];
//
//        String setence = inputDoUsuario.readLine();
//        sendData = setence.getBytes();
//
//        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAdress, 5000);
//        clienteSocket.send(sendPacket);
//
//        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
//        clienteSocket.receive(receivePacket);
//
//        String modifiedSetence = new String(receivePacket.getData());
//
//        System.out.println("FROM SERVER: " + modifiedSetence);
//        clienteSocket.close();
    }

    private static void registraJogador() throws IOException {
        DatagramSocket clienteSocket = new DatagramSocket();
        InetAddress IPServidor = InetAddress.getByName(enderecoServidor);
        byte[] bufferMensamRecebida = new byte[TAMANHO_MENSAGEM];

        //Envia solicitação de registro de jogador.
        DatagramPacket registroJogador = new DatagramPacket(REGISTRO_JOGADOR.getBytes(), REGISTRO_JOGADOR.getBytes().length, IPServidor, portaDeAcesso);
        clienteSocket.send(registroJogador);

        //Aguarda solicitação de dados do jogador para seguir com registro.
        DatagramPacket registroServidor = new DatagramPacket(bufferMensamRecebida, bufferMensamRecebida.length);
        clienteSocket.receive(registroServidor);

        System.out.println(registroJogador.getLength());
    }

    private static void criaJogador() throws IOException {
        BufferedReader inputDoUsuario = new BufferedReader(new InputStreamReader(System.in));

        //Obtém o nome do jogador.
        slowPrint("Digite o seu nome de jogador:\n", INTERVALO_DIGITACAO);
        String nomeJogador = inputDoUsuario.readLine();

        //Obtém o nome da nave.
        slowPrint("Digita o nome da sua nave:\n", INTERVALO_DIGITACAO);
        String nomeNave = inputDoUsuario.readLine();

        //Cria o jogador.
        jogador = new PlayerCliente(nomeJogador, nomeNave);
    }

    private static void enderecoDeAcesso() throws IOException {
        BufferedReader inputDoUsuario = new BufferedReader(new InputStreamReader(System.in));

        //Obtém o endereço de acesso do servidor.
        slowPrint("Digite o endereço de acesso do servidor:\n", INTERVALO_DIGITACAO);
        String endereco = inputDoUsuario.readLine();

        //Verifica se é utilizado como endereço o nome "localhost" em formato case insensitive, trocando o mesmo pelo endereço IP localhost.
        if ((endereco == null || endereco.isEmpty())) enderecoServidor = LOCALHOST_ANDRESS;

        //Salva endereço do servidor.
        enderecoServidor = endereco;

        //Obtém a porta de acesso do servidor.
        slowPrint("Digite a porta de acesso do servidor:\n", INTERVALO_DIGITACAO);
        String porta = inputDoUsuario.readLine();

        //Verifica se a porta digita é nula ou vazia, sendo utilizada nesse caso a porta padrão (5000).
        if (porta.isEmpty() || porta == null) porta = PORTA_PADRAO;

        //Salva porta de acesso ao servidor.
        portaDeAcesso = Integer.parseInt(porta);
    }

    /**
     * SlowPrint cria o efeito de digitação em tela de console, enquanto printa a mensagem.
     * https://www.youtube.com/watch?v=7DCSDD-UspA
     *
     * @param message Texto a ser exibido em tela.
     * @param milliseconds Intervalo de tempo entre digitação de cada letra
     */
    public static void slowPrint(String message, long milliseconds) {
        for (int i = 0; i < message.length(); i++) {
            System.out.print(message.charAt(i));

            try {
                Thread.sleep(milliseconds);
            } catch (InterruptedException erro) {
                erro.printStackTrace();
            }
        }
    }
}
