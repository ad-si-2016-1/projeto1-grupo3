package objetos;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PlayerServer {
    private byte[] ID;

    private String nomeJogador;
    private String nomeNave;
    private Long momentoCriadoPlayer;

    private int posicaoHorizontal = 0;
    private int posicaoVertical = 0;
    private int vidas = 3;

    /**
     * Criar um jogador para ser utilizado pelo SocketWar. Sendo necessário ao criar um jogador cadastrar o nome da sua
     * name e o nome do jogador.
     *
     * NOTA: Para segurança cada jogador possui um ID único a ser utilizado para registro no jogo, sendo esse ID o resultado
     * de uma operação com o nome do jogador, nome da nave e o momento em que o player é cadastrado no jogo. Ou seja, a única
     * forma de existe jogadores identicos (clones) em jogo é se ambos tiverem o mesmo nome, mesmo nome para a nave e forem
     * criados no mesmo instante de tempo em millisegundos.
     *
     * @param nomeJogador No jogador.
     * @param nomeNave Nome da nave do jogador.
     * @throws IOException Erro disparado quando se apresenta algum tipo de problema com o nome do jogador ou nome da nave.
     */
    public PlayerServer(String nomeJogador, String nomeNave) throws IOException {
        //Lança exceção caso o nome da nave ou do jogador são nulos/vazios.
        if ((nomeJogador == null || nomeJogador.isEmpty()) ||
                (nomeNave == null || nomeNave.isEmpty())) {
            throw new IOException("Nome do jogador e da nave não podem ser nulos/vazios");
        }

        //Salva os três principais dados do Player em memória.
        this.nomeJogador = nomeJogador;
        this.nomeNave = nomeNave;
        momentoCriadoPlayer = System.currentTimeMillis();

        //Junta todos os arrays de bytes em um único para criação do ID do jogador.
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        byteStream.write(this.nomeJogador.getBytes());
        byteStream.write(this.nomeNave.getBytes());
        byteStream.write(momentoCriadoPlayer.byteValue());

        //Cria ID do jogador a partir da união dos principais dados do jogador, utilizando o MD5 para isso.
        try {
            MessageDigest MD5 = MessageDigest.getInstance("MD5");
            ID = MD5.digest(byteStream.toByteArray());
        } catch (NoSuchAlgorithmException erro) {
            System.out.println(erro.toString());
            ID = null;
        }
    }

    /**
     * Define a posição horizontal do jogador.
     * @param x posição horizontal do jogador.
     */
    public void setPosicaoHorizontal (int x) {
        posicaoHorizontal = x;
    }

    /**
     * Define a posição vertical do jogador.
     * @param y posição vertical do jogador.
     */
    public void setPosicaoVertical (int y) {
        posicaoVertical = y;
    }

    /**
     * Define as vidas do jogador.
     * @param vidas quantidade de vidas do jogador.
     */
    public void setVidas (int vidas) {
        this.vidas = vidas;
    }

    /**
     * Obtém a atual posição horizontal (x) do jogador.
     * @return Posição Horizontal (x) do jogador.
     */
    public int getPosicaoHorizontal () {
        return posicaoHorizontal;
    }

    /**
     * Obtém a atual posição vertical (y) do jogador.
     * @return Posição Vertical (y) do jogador.
     */
    public int getPosicaoVertical () {
        return posicaoVertical;
    }

    /**
     * Obtém a quantidade de vidas do jogador.
     * @return Quantidade de vidas do jogador.
     */
    public int getVidas() {
        return vidas;
    }

    /**
     * Obtém o ID do jogador.
     * @return ID do jogador.
     */
    public byte[] getID() {
        return ID;
    }
}
