import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Servidor {
    public static int porta = 5000;
    public static int tamanhoPacote = 1024; //Tamanho do pacote UDP em bytes

    public static void main(String args[]) throws IOException {
        DatagramSocket serverSocket = new DatagramSocket(porta);
        byte[] dadosRecebidos = new byte[tamanhoPacote];
        byte[] dadosEnviados = new byte[tamanhoPacote];

        while(true) {
            DatagramPacket pacoteRecebido = new DatagramPacket(dadosRecebidos, dadosRecebidos.length);
            serverSocket.receive(pacoteRecebido);

            String sequencia = new String(pacoteRecebido.getData());
            System.out.println("RECEBEIDO: " + sequencia);

            InetAddress IPAddress = pacoteRecebido.getAddress();
            int porta = pacoteRecebido.getPort();

            String upperCaseSequencia = sequencia.toUpperCase();

            dadosEnviados = upperCaseSequencia.getBytes();

            DatagramPacket pacoteEnviado = new DatagramPacket(dadosEnviados, dadosEnviados.length, IPAddress, porta);
            serverSocket.send(pacoteEnviado);
        }
    }
}
