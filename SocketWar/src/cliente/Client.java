package cliente;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Client extends JFrame{
	
	private static final long serialVersionUID = 1L;
	
	JTextArea textoRecebido;
	JTextField txtCoordenadaAtaque;
	JTextField txtCoordenadaDefesa;
	PrintWriter escritor;
	Socket socket;
	String nome;
	Scanner leitor;
	
	private class EscutaServidor implements Runnable{

		@Override
		public void run() {
			try{
				String texto;
				while((texto = leitor.nextLine()) != null){
					textoRecebido.append(texto + "\n");
				}
			}catch (Exception e){
				e.printStackTrace();
			}
			
		}
		
	}
	
	public Client(String nome){
		super("Jogador/Client: " + nome);
		
		this.nome = nome;
		
		txtCoordenadaAtaque = new JTextField();
		txtCoordenadaDefesa = new JTextField();
		JButton botao = new JButton("Enviar");
		botao.addActionListener(new EnviarListener());
		
		Container envio = new JPanel();
		envio.setLayout(new BorderLayout());
		envio.add(BorderLayout.NORTH, txtCoordenadaAtaque);
		envio.add(BorderLayout.CENTER, txtCoordenadaDefesa);
		envio.add(BorderLayout.SOUTH, botao);
			
		textoRecebido = new JTextArea();
		JScrollPane scroll = new JScrollPane(textoRecebido);
		
		getContentPane().add(BorderLayout.CENTER, scroll);
		getContentPane().add(BorderLayout.SOUTH, envio);
		
		configurarRede();
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(500, 500);
		
		setVisible(true);
	}
	
	private class EnviarListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			escritor.println(nome);
			escritor.println("Ataque: " + txtCoordenadaAtaque.getText());
			escritor.println("Defesa: " + txtCoordenadaDefesa.getText());
			escritor.flush();
			txtCoordenadaAtaque.setText(null);
			txtCoordenadaDefesa.setText(null);
			txtCoordenadaAtaque.requestFocus();
		}
		
	}

	public void configurarRede(){
		try {
			
			socket = new Socket("localhost", 5000);
			
			escritor = new PrintWriter(socket.getOutputStream());
			
			leitor = new Scanner(socket.getInputStream());
			
			new Thread(new EscutaServidor()).start();
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		new Client("1");
		new Client("2");
	}

}