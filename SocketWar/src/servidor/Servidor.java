package servidor;

import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Servidor {

	List<PrintWriter> escritores = new ArrayList<>();

	public Servidor() {
		ServerSocket server;
		try {
			server = new ServerSocket(5000);

			while(true){
				Socket socket = server.accept();
				new Thread(new EscutaCliente(socket)).start();
				PrintWriter p = new PrintWriter(socket.getOutputStream());
				escritores.add(p);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void encaminharParaTodos(String texto){
		for (PrintWriter w : escritores) {
			try {
//				w.println("_1_2_3_4_5_6_7_8_9");
//				for (int i = 0; i < 5; i++) {
//					w.println("| _ _ _ _ _ _ _ _ _ _ _ _ |");
//				}
//				w.println("_1_2_3_4_5_6_7_8_9");
				w.println(texto);
				w.flush();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	private class EscutaCliente implements Runnable{

		Scanner leitor;

		public EscutaCliente(Socket socket){
			try {
				leitor = new Scanner(socket.getInputStream());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void run() {

			String texto;

			while((texto = leitor.nextLine()) != null){
				System.out.println("Recebeu: " + texto);
				encaminharParaTodos(texto);
			}
		}

	}

	public static void main(String[] args) {
		new Servidor();
	}

}