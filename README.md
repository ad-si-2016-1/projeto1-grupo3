# Grupo 3

## Integrantes:
* Rhandy (Líder)
* Felipe (Programador)
* Gleydson (Programador)
* Paulo Henrique (Documentador)
* Alexandre Pinheiro (Programador)

## Nome do Jogo
Socket War

## Descrição e Regras
Dois jogadores irão se enfrentar em uma batalha espacial com o objetivo de se destruírem. Inicialmente os jogadores terão que se conectar na sala
disponibilizada pelo servidor. Serão apenas dois jogadores por sala, o objetivo dos jogadores é se matar. Cada jogador terá disparos
ilimitados e a rodada acaba quando um deles atingir 3 pontos, para pontuar o jogador deve acertar 1 disparo em seu oponente e cada jogador
efetua seu disparo em sua vez não sendo possível os dois atirarem ao mesmo tempo. No fim da rodada o  servidor irá informar o vencedor e a sala será fechada.


## Quantos Jogam?
Limite de 2 jogadores.